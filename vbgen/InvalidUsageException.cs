using System;

namespace VbGen
{
    public class InvalidUsageException : Exception
    {
        public InvalidUsageException()
            : base("Invalid usage - run `vbgen help`") { }

        public InvalidUsageException(string message)
            : base($"Invalid usage: {message} - run `vbgen help`") { }
    }
}