﻿using System;
using System.Linq;
using VbGen.Generators.Project;

namespace VbGen
{
    public static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length < 1)
                    throw new InvalidUsageException("Too few arguments");

                var subCommand = args.First();
                switch (subCommand)
                {
                    case "help":
                        Help.PrintUsage();
                        return;
                    
                    case "vbp":
                        ProjectGenerator.GenerateProject(args);
                        return;
                    
                    default:
                        throw new NotSupportedException($"Unknown subcommand: {subCommand}");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred:");
                Console.WriteLine(e);
            }
        }
    }
}