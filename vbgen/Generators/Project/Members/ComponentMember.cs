using System;
using vbgen.Generators.Project.Members;

namespace VbGen.Generators.Project.Members
{
    internal class ComponentMember : ComMember
    {
        public string ObjectName { get; }
        public override string Key => "Object";
        public override string Value => $"{{{ClsId}}}#{ClsVersion}#0; {ObjectName}";

        public ComponentMember(string projectPath, string objectName)
            : base(projectPath, GetObjectPath(objectName))
        {
            ObjectName = objectName;
        }

        private static string GetObjectPath(string objectName)
        {
            throw new NotImplementedException("Don't know how to find COM components!");
        }
    }
}