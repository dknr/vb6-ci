using System.IO;

namespace vbgen.Generators.Project.Members
{
    internal abstract class FileMember : ProjectMember
    {
        public override string Value => RelativePath;
        public string RelativePath { get; }

        protected FileMember(string projectPath, string memberPath)
            : this(Path.GetRelativePath(projectPath, memberPath)) { }
        
        protected FileMember(string relativePath)
            => RelativePath = relativePath;
    }
}