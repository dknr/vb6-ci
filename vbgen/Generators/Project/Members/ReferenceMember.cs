using System.IO;
using System.Net;

namespace vbgen.Generators.Project.Members
{
    internal class ReferenceMember : ComMember
    {
        public ReferenceMember(string projectPath, string memberPath)
            : base(projectPath, memberPath) { }

        public override string Key => "Reference";
        public override string Value => $"*\\{{{ClsId}}}#{ClsVersion}#0#{RelativePath}";
    }
}