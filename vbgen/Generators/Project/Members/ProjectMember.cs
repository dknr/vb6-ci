using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using VbGen.Generators.Project.Members;

namespace vbgen.Generators.Project.Members
{
    internal abstract class ProjectMember
    {
        public static ProjectMember FromPath(string projectPath, string memberPath)
        {
            var ext = System.IO.Path.GetExtension(memberPath).ToLower();
            switch (ext)
            {
                case "dll":
                    return new ReferenceMember(projectPath, memberPath);
                    
                case "ocx":
                    return new ComponentMember(projectPath, memberPath);
                    
                case "frm":
                    return new FormMember(projectPath, memberPath);
                    
                case "bas":
                    return new ModuleMember(projectPath, memberPath);
                
                case "cls":
                    return new ClassMember(projectPath, memberPath);
                    
                default:
                    throw new NotSupportedException($"Unknown file type: {ext}");
            }
        }
        
        public abstract string Key { get; }
        public abstract string Value { get; }

        public override string ToString()
            => $"{Key}={Value}";
    }

    internal abstract class VbNamedFileMember : FileMember
    {
        public string VbName { get; }
        
        protected VbNamedFileMember(string projectPath, string memberPath) 
            : base(projectPath, memberPath)
        {
            if (File.Exists(memberPath)) return;
            
            Console.WriteLine($"Warning: assuming VB Name for {memberPath}");    // TODO: implement global logger
            VbName = Path.GetFileNameWithoutExtension(memberPath);
        }
        
        public override string Value => $"{VbName}; {RelativePath}";
    }

    internal class ClassMember : VbNamedFileMember
    {
        public ClassMember(string projectPath, string memberPath)
            : base(projectPath, memberPath) { }

        public override string Key => "Class";
    }

    internal class ModuleMember : VbNamedFileMember
    {
        public ModuleMember(string projectPath, string memberPath)
            : base(projectPath, memberPath) { }

        public override string Key => "Module";
    }

    internal class FormMember : FileMember
    {
        public override string Key => "Form";

        public FormMember(string projectPath, string memberPath)
            : base(projectPath, memberPath) { }
    }

    internal enum ProjectMemberType
    {
        Reference,
        Object,
        Form,
        Module,
        Class
    }
}