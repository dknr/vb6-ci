using System;
using System.IO;

namespace vbgen.Generators.Project.Members
{
    internal abstract class ComMember : FileMember
    {
        public string ClsId { get; }
        public string ClsVersion { get; }

        protected ComMember(string projectPath, string memberPath)
            : base(projectPath, memberPath)
        {
            if (!File.Exists(memberPath))
                return;
            
            throw new NotImplementedException("Don't know how to parse CLSID from COM DLL yet!");
        }
    }
}