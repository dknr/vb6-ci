using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using vbgen.Generators.Project.Members;
using VbGen.Generators.Project.Members;

namespace VbGen.Generators.Project
{
    internal struct Options
    {
        public Options(string[] args)
        {
            ProjectType = ProjectType.Exe;
            
            ProjectPath = args.FirstOrDefault()
                          ?? throw new InvalidUsageException("Project path not specified");
            
            var projectMembers = new List<ProjectMember>();
            var argsQueue = new Queue<string>(args.Skip(1));
            
            while (argsQueue.TryDequeue(out var arg))
            {
                switch (arg)
                {
                    case "-t":
                    case "--project-type":
                        ProjectType = argsQueue.Dequeue().AsEnum<ProjectType>();
                        break;
                        
                    case "-o":
                    case "--object-member":
                        var objectName = argsQueue.Dequeue();
                        projectMembers.Add(new ComponentMember(ProjectPath, objectName));
                        break;
                        
                    default:
                        projectMembers.Add(ProjectMember.FromPath(ProjectPath, arg));
                        break;
                }
            }

            ProjectMembers = projectMembers.ToImmutableList();
        }

        public ProjectType ProjectType { get; }
        public string ProjectPath { get; }
        public IEnumerable<ProjectMember> ProjectMembers { get; }
    }
}