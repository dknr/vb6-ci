using System;

namespace VbGen
{
    internal static class StringExtensions
    {
        public static T AsEnum<T>(this string value) where T : struct
        {
            return Enum.Parse<T>(value);
        }
    }
}