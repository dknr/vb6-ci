#!/bin/bash

export DISPLAY=:0
Xvfb $DISPLAY -auth ~/.Xauthority -screen 0 1024x768x24 >> ~/xvfb.log 2>&1 &

x11vnc -forever -nopw &
xeyes &

wineboot

cd ~/.wine/drive_c/VB98
wine cmd