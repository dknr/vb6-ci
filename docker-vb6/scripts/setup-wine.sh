#!/bin/bash
set -ex

export DISPLAY=:0
Xvfb $DISPLAY -auth ~/.Xauthority -screen 0 1024x768x24 &
XVFB_PID=$!

wineboot --init
wineboot -u

winetricks -q vcrun6
winetricks -q mfc40

wineboot -s

kill $XVFB_PID