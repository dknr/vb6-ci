#!/bin/sh

set -ex

mkdir -p deps
cd deps

curl -LO https://dl.winehq.org/wine-builds/winehq.key
curl -LO https://dl.winehq.org/wine/wine-mono/4.7.5/wine-mono-4.7.5.msi
curl -LO https://dl.winehq.org/wine/wine-gecko/2.47/wine_gecko-2.47-x86.msi
curl -LO https://download.microsoft.com/download/vc60pro/Update/2/W9XNT4/EN-US/VC6RedistSetup_deu.exe
curl -LO https://download.microsoft.com/download/0/A/F/0AFB5316-3062-494A-AB78-7FB0D4461357/windows6.1-KB976932-X86.exe