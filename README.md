# Introduction
This is a futile attempt at creating a CI/CD pipeline for Visual Basic 6 using WINE and Docker. It does not entirely work... yet.

# Prerequisites - docker-vb6
- Docker
- One `$CHICKEN`
- cURL
- Visual Studio 6.0 CD1

# Instructions - docker-vb6
- `git clone https://gitlab.com/dknr/vb6-ci`
- `cd vb6-ci/docker-vb6`
- `sacrifice --diety=cthulu $CHICKEN`
- `sh ./update-deps.sh`
- `cp -a path/to/visual-studio-6/CD1 deps/VS60-CD1`
- `docker build .`
- `docker run --rm -p 5900:5900 -it <IMAGE_ID>`

If successful, you will be greeted by a CMD prompt. Attach a VNC client to the container, then run `VB6.EXE`. It *will* crash. 

If the build fails, then the gods were not pleased by your sacrifice. Try again with a `$CHICKEN` of higher quality, or perhaps choose another diety for the sacrifice. If you're only testing the build, please remember to mount a scratch monkey to avoid wasting valuable resources.

# `TODO`
- add instructions for vbgen project

# Acknowlegements
I'm thankful for [telyn's](https://github.com/telyn) [docker-vb6](https://github.com/telyn/docker-vb6) and [docker-wine-vcrun6](https://github.com/telyn/docker-wine-vcrun6) repositories, which provided the inspiration for this project and ultimately convinced me that dockerizing a two-decade old compiler isn't total batshit. 